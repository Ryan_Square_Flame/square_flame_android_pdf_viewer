var argscheck = require('cordova/argscheck'),
    utils = require('cordova/utils'),
    exec = require('cordova/exec');

    
var PLUGIN_NAME = "PDFViewer";
    
var PDFViewer = function() {};

PDFViewer.setup = function(PageUrl, paddingTop, paddingBottom, resWidth, resHeight, successCallback, errorCallback) 
{
  exec(successCallback, errorCallback, PLUGIN_NAME, "setupview", [PageUrl, paddingTop, paddingBottom, resWidth, resHeight]);
};
PDFViewer.render = function(PageNo, successCallback, failureCallback) 
{
  exec(successCallback, failureCallback, PLUGIN_NAME, "render", [PageNo]);
};
PDFViewer.closerender = function(successCallback, errorCallback)
{
	exec(successCallback, errorCallback, PLUGIN_NAME, "removerenderview", []);
}
PDFViewer.addswipecallback = function(successCallback)
{
	exec(successCallback, null, PLUGIN_NAME, "swipecallback", []);
}

module.exports = PDFViewer;