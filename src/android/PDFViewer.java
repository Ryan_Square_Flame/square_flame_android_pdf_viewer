package org.apache.cordova.pdfviewer;

import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.apache.cordova.LOG;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.view.View;
import android.view.ViewGroup;
import android.view.Gravity;
import android.app.Dialog;
import android.content.DialogInterface;

import android.graphics.Bitmap;
import android.graphics.pdf.PdfRenderer;
import android.graphics.pdf.PdfRenderer.Page;
import android.os.ParcelFileDescriptor;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileInputStream;
import android.util.DisplayMetrics;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.widget.ImageView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.FrameLayout.LayoutParams;
import java.lang.System;

public class PDFViewer extends CordovaPlugin {
	
  private final String TAG = "PDFViewer";
	private LinearLayout windowLayer = null;
	private ZoomableImageView zoomView = null;
	private ViewGroup root;
	private File file = null;
	private int currentPage = 1;
	private boolean bRendering = false;
	int REQ_WIDTH = 0;
	int REQ_HEIGHT = 0;
	int PageCount = 0;
	InputStream in = null;
	OutputStream out = null;
	CallbackContext SwipeCallback = null;
	PdfRenderer renderer = null;
	Page pageToRender = null;
						
  public PDFViewer(){
		super();
	}
   
	@Override
    public void initialize(final CordovaInterface cordova, CordovaWebView webView) {
			super.initialize(cordova, webView);

			root = (ViewGroup) webView.getView().getParent();
    }
		
		@Override
		public boolean execute(final String action, final JSONArray args, final CallbackContext callbackContext) throws JSONException {
			
			final PDFViewer _this = this;
      
			if("setupview".equals(action)) {
				cordova.getActivity().runOnUiThread(new Runnable() {
					public void run() {
						// window layout
						windowLayer = new LinearLayout(cordova.getActivity());

						LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
						layoutParams.gravity = Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL;
						windowLayer.setLayoutParams(layoutParams);
						
						layoutParams = null;
						
						windowLayer.setTag(123);

						zoomView = new ZoomableImageView(cordova.getActivity());
						zoomView.setParent(_this);
						//zoomView.setLayoutParams(layoutParams);
						zoomView.setDefaultScale(1);
						zoomView.setTag(1234);	
						
						try
            {
							file = new File(cordova.getActivity().getApplicationContext().getFilesDir(), args.getString(0));

							renderer = new PdfRenderer(ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY));
							PageCount = renderer.getPageCount();
							
							
							
							windowLayer.setPadding(0, args.getInt(1), 0, (PageCount > 1 ? args.getInt(2) : 0));
							REQ_WIDTH = args.getInt(3);// - (args.getInt(1) + args.getInt(2));
							REQ_HEIGHT = args.getInt(4);// - (args.getInt(1) + args.getInt(2));
							windowLayer.addView(zoomView);
							root.addView(windowLayer, 1);
							zoomView = null;
							windowLayer = null;
							callbackContext.success(PageCount);
						}
						catch(Exception e)
						{	
							//file = null;
							zoomView = null;
							windowLayer = null;
							callbackContext.error(e.getMessage());
						}
					}
				});
				return true;
			}
			else if ("render".equals(action)) {
				cordova.getActivity().runOnUiThread(new Runnable() {
					public void run() {
						boolean bPortrait = true;
						int imgWidth = 1280;
						int imgHeight = 1920;
						float diffRatioX = 0.0f;
						float diffRatioY = 0.0f;
						boolean bAllowSlideLeft = false;
						boolean bAllowSlideRight = false;
						
						try{
							
							pageToRender = renderer.openPage(args.getInt(0));

							if(pageToRender.getWidth() > pageToRender.getHeight())
								bPortrait = false;
							
							if(!bPortrait){
								imgWidth = 1920;
								imgHeight = 1280;
							}
								
							if(bPortrait)
							{
								if(REQ_WIDTH < imgWidth && REQ_HEIGHT < imgHeight)
								{
									diffRatioX = ((float)REQ_WIDTH / (float)imgWidth);
									diffRatioY = ((float)REQ_HEIGHT / (float)imgHeight);
									
									imgWidth = Math.round(imgWidth * diffRatioX);
									imgHeight = Math.round(imgHeight * diffRatioY);
								}
							}

							
							Bitmap bitmap = Bitmap.createBitmap(imgWidth, imgHeight, Bitmap.Config.ARGB_8888);
							Rect rect = new Rect(0,0, imgWidth, imgHeight);
							
							if(args.getInt(0) < PageCount - 1)
								bAllowSlideRight = true;
							if(args.getInt(0) > 0)
								bAllowSlideLeft = true;

							if(pageToRender != null) {
									pageToRender.render(bitmap, rect, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
									pageToRender.close();
							}
							if(bitmap != null) {
									((ZoomableImageView)root.findViewWithTag(1234)).setImageBitmap(bitmap, bAllowSlideLeft, bAllowSlideRight);
							}
							bRendering = false;
							
							bitmap = null;
							rect = null;
							System.gc();
							
							callbackContext.success("File Success");
								
            } catch (Exception e)
            {
                callbackContext.error(e.getMessage());
								bRendering = false;
            }
					}
				});
				return true;
			}
			else if ("removerenderview".equals(action)) {
				cordova.getActivity().runOnUiThread(new Runnable() {
					public void run() {
						try{
							((LinearLayout)root.findViewWithTag(123)).removeAllViews();
							root.removeView(root.findViewWithTag(123));
							file = null;
							renderer.close();
							renderer = null;
							pageToRender = null;
							System.gc();
							callbackContext.success("View Removed");
						}
						catch(Exception e){
							callbackContext.error(e.getMessage());
						}
					}
				});
				return true;
			}
			else if("swipecallback".equals(action)){
				cordova.getActivity().runOnUiThread(new Runnable() {
					public void run() {
						SwipeCallback = callbackContext;
					}
				});
				return true;
			}
			else{
				callbackContext.error("render not found");
        return false;
			}
		}
		public void CallSwipeFunction(int dir)
		{
			SwipeCallback.success(dir);
		}
		private void copyFile(InputStream in, OutputStream out) throws JSONException
		{
				byte[] buffer = new byte[1024];
				int read;
				try{
					while ((read = in.read(buffer)) != -1)
					{
						out.write(buffer, 0, read);
					}
				}
				catch (Exception e)
				{
					
				}
		}
		private View cachedView;
		private View getView() {
    if (cachedView == null) {
      try {
        cachedView = (View) webView.getClass().getMethod("getView").invoke(webView);
      } catch (Exception e) {
        cachedView = (View) webView;
      }
    }
    return cachedView;
  }
}